fapolicyd (1.3.2-1) UNRELEASED; urgency=medium

  [ Fukui Daichi ]
  * New upstream version 1.3.2 (Closes: #1037547)
  * Remove applied patches
  * Fix patch to adapt to the new upstream
  * Fix undefined reference to md5 functions
  * Allow the deb database to be a trust source
  * d/control: Add libdpkg-dev to build for the deb database

 -- Fukui Daichi <a.dog.will.talk@akane.waseda.jp>  Mon, 14 Aug 2023 07:52:13 +0000

fapolicyd (1.1.7-5) unstable; urgency=medium

  * d/rules: Add --with-systemdsystemunitdir to dh_auto_configure

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Sun, 16 Apr 2023 20:15:04 +0900

fapolicyd (1.1.7-4) unstable; urgency=medium

  * Fix install path of systemd service file from /usr/lib/systemd/system
    to /usr/lib/systemd . (Closes: #1034238)
    Add d/patches/0007-Install-service-file-to-usr-lib-systemd.patch

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Sun, 16 Apr 2023 19:34:20 +0900

fapolicyd (1.1.7-3) unstable; urgency=medium

  * d/fapolicyd.postinst: Create /var/lib/fapolicyd. (Closes: #1030968)
  * d/fapolicyd.postrm: Remove /var/lib/fapolicyd.

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Thu, 02 Mar 2023 06:54:12 +0900

fapolicyd (1.1.7-2) unstable; urgency=medium

  [ Fukui Daichi ]
  * Move manpage for fapolicyd-cli from section 1 to 8
  * Rediff patches
  * Add d/patches/0005-Remove-the-man1-file-entry.patch
  * Fix spelling errors
  * d/rules: remove empty directories
  * d/control: make description field more detailed

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Tue, 07 Feb 2023 09:49:57 +0900

fapolicyd (1.1.7-1) unstable; urgency=medium

  * Initial release. (Closes: #1022947)

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Fri, 28 Oct 2022 17:32:38 +0900
